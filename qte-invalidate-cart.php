<?php
/**
* Plugin Name: QTE Invalidate Cart
* Description: This plugin invalidates the cart when the language switches.
* Author: QTE Development AB
* Author URI: https://qte.se
* Version: 1.0.0
*/

add_action('wcml_user_switch_language', function(){
    add_action('wp_loaded', function (){
    	global $woocommerce;
    	$woocommerce->cart->empty_cart();
	$woocommerce->session->destroy_session();
    });
});

